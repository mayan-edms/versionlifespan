from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import DocumentVersionLifespanEditView

urlpatterns = patterns(
    '',
    url(
        r'^(?P<pk>\d+)/$',
        DocumentVersionLifespanEditView.as_view(),
        name='document_version_lifespan_edit'
    ),
)
