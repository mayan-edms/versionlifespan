from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from navigation import Link

from .permissions import permission_lifespan_edit

link_lifespan_edit = Link(
    permissions=(permission_lifespan_edit,), text=_('Edit lifespan'),
    view='versions_lifespans:document_version_lifespan_edit', args='object.id'
)
