# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0028_newversionblock'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentVersionLifespan',
            fields=[
                (
                    'id', models.AutoField(
                        verbose_name='ID', serialize=False, auto_created=True,
                        primary_key=True
                    )
                ),
                (
                    'expiration_datetime', models.DateTimeField(
                        blank=True, null=True,
                        verbose_name='Expiration date and time',
                    )
                ),
                (
                    'document_version', models.OneToOneField(
                        related_name='lifespan',
                        verbose_name='Document version',
                        to='documents.DocumentVersion'
                    )
                ),
            ],
            options={
                'verbose_name': 'Document version lifespan',
                'verbose_name_plural': 'Document versions lifespans',
            },
            bases=(models.Model,),
        ),
    ]
