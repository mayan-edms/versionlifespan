# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def initialize_lifespans(apps, schema_editor):
    """Initialize lifespan for existing document versions"""

    DocumentVersionLifespan = apps.get_model(
        'versions_lifespans', 'DocumentVersionLifespan'
    )
    DocumentVersion = apps.get_model('documents', 'DocumentVersion')

    for document_version in DocumentVersion.objects.all():
        DocumentVersionLifespan.objects.create(document_version=document_version)


def destroy_lifespans(apps, schema_editor):
    DocumentVersionLifespan = apps.get_model(
        'versions_lifespans', 'DocumentVersionLifespan'
    )

    DocumentVersionLifespan.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('versions_lifespans', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(initialize_lifespans, destroy_lifespans),
    ]
