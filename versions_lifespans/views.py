from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

from common.views import SingleObjectEditView

from .models import DocumentVersionLifespan

from .permissions import permission_lifespan_edit


class DocumentVersionLifespanEditView(SingleObjectEditView):
    fields = ('expiration_datetime',)
    model = DocumentVersionLifespan
    view_permission = permission_lifespan_edit

    def get_post_action_redirect(self):
        return reverse(
            'documents:document_version_list',
            args=(self.get_object().document_version.document.pk,)
        )

    def get_object(self):
        return get_object_or_404(
            DocumentVersionLifespan, document_version__pk=self.kwargs['pk']
        )

    def get_extra_context(self):
        return {
            'object': self.get_object().document_version.document,
            'title': _(
                'Edit lifespan for document version: %s'
            ) % self.get_object(),
        }
