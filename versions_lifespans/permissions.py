from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from permissions import PermissionNamespace

namespace = PermissionNamespace('versions_lifespans', _('Version lifespans'))

permission_lifespan_edit = namespace.add_permission(
    name='lifespan_edit', label=_('Edit document version\'s lifespans')
)
