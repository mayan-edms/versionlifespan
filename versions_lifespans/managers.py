from __future__ import unicode_literals

import logging

from django.db import models
from django.utils.timezone import now

from documents.models import DocumentVersion

logger = logging.getLogger(__name__)


class DocumentVersionLifespanManager(models.Manager):
    def expired(self):
        """
        Model manager method that returns a queryset of all the expired
        document version in the system.
        """

        return DocumentVersion.objects.filter(
            lifespan__expiration_datetime__lt=now()
        )
