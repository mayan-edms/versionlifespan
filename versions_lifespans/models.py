from __future__ import unicode_literals

import logging

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from documents.models import DocumentVersion

from .managers import DocumentVersionLifespanManager

logger = logging.getLogger(__name__)


@python_2_unicode_compatible
class DocumentVersionLifespan(models.Model):
    """
    Model to store the lifespan as an expiration date for each document version
    in the system.
    """

    document_version = models.OneToOneField(
        DocumentVersion, related_name='lifespan',
        verbose_name=_('Document version')
    )

    expiration_datetime = models.DateTimeField(
        blank=True, null=True, verbose_name=_('Expiration date and time')
    )

    objects = DocumentVersionLifespanManager()

    class Meta:
        verbose_name = _('Document version lifespan')
        verbose_name_plural = _('Document versions lifespans')

    def __str__(self):
        return unicode(self.document_version)
