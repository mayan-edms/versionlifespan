from __future__ import unicode_literals

from .models import DocumentVersionLifespan


def initialize_lifespan(sender, instance, **kwargs):
    """Create a lifespan entry for each newly created document version"""
    DocumentVersionLifespan.objects.create(document_version=instance)
