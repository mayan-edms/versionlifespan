from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from common import menu_object
from common.apps import MayanAppConfig
from documents.models import DocumentVersion
from documents.signals import post_version_upload

from navigation import SourceColumn

from .handlers import initialize_lifespan
from .links import link_lifespan_edit


class VersionsLifespansApp(MayanAppConfig):
    name = 'versions_lifespans'
    test = True
    verbose_name = _('Document versions lifespans')

    def ready(self):
        super(VersionsLifespansApp, self).ready()

        # Add expiration date column to the DocumentVersion model
        SourceColumn(
            source=DocumentVersion, label=_('Expiration'),
            func=lambda context: context['object'].lifespan.expiration_datetime
        )

        menu_object.bind_links(
            links=(link_lifespan_edit,), sources=(DocumentVersion,)
        )

        # Initialize the lifespan of each newly created document version
        post_version_upload.connect(
            initialize_lifespan, dispatch_uid='initialize_lifespan',
            sender=DocumentVersion
        )
