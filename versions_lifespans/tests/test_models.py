from __future__ import unicode_literals

from datetime import datetime, timedelta

from django.core.files.base import File
from django.test import TestCase, override_settings

from documents.models import DocumentType
from documents.tests import TEST_DOCUMENT_TYPE, TEST_SMALL_DOCUMENT_PATH

from ..models import DocumentVersionLifespan


@override_settings(OCR_AUTO_OCR=False)
class TagTestCase(TestCase):
    def setUp(self):
        self.document_type = DocumentType.objects.create(
            label=TEST_DOCUMENT_TYPE
        )

        with open(TEST_SMALL_DOCUMENT_PATH) as file_object:
            self.document = self.document_type.new_document(
                file_object=File(file_object)
            )

    def tearDown(self):
        self.document.delete()
        self.document_type.delete()

    def test_empty_expired_queryset(self):
        self.assertQuerysetEqual(
            DocumentVersionLifespan.objects.expired(), ()
        )

    def test_expired_queryset(self):
        document_version = self.document.latest_version
        document_version.lifespan.expiration_datetime = datetime(
            year=1970, month=1, day=1
        )
        document_version.lifespan.save()

        self.assertQuerysetEqual(
            DocumentVersionLifespan.objects.expired(),
            (repr(self.document.latest_version),)
        )

    def test_no_expired_future_queryset(self):
        document_version = self.document.latest_version
        document_version.lifespan.expiration_datetime = datetime.now() + timedelta(days=1)
        document_version.lifespan.save()

        self.assertQuerysetEqual(
            DocumentVersionLifespan.objects.expired(), ()
        )
