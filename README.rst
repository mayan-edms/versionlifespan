Description
-----------
Mayan EDMS app that adds document versions lifespan support.

License
-------
This project is open sourced under the `MIT License`_.

.. _`MIT License`: https://gitlab.com/mayan-edms/versionlifespan/raw/master/LICENSE


Installation
------------
Clone repository in a directory outside of Mayan EDMS::

    git clone https://gitlab.com/mayan-edms/versionlifespan.git

Symlink the app into your Mayan EDMS' app folder::

    ln -s <repository directory>/versions_lifespans/ <Mayan EDMS directory>/mayan/apps

Add `versions_lifespans` to your `INSTALLED_APPS` list.

Run the migrations for the app::

    ./manage.py migrate
